import {combineReducers} from 'redux';
import auth from '../../features/login/reducer_login';
import getBooks from '../../features/dashboard/reducer_dashboard';

export default combineReducers({
	auth,
	getBooks
});
