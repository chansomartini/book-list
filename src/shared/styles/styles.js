import {moderateScale} from 'react-native-size-matters';
import {Dimensions, PixelRatio, StatusBar} from 'react-native';

const AppStyle = {
  color: {
    primary: '#05ba74',
    purple: '#bd72de',
    blue: '#44b0f9',
    red: '#f89f85',
    dark: '#000',
    light: '#fff',
  },
  fontPoppins: {
    regular: 'Poppins-Regular',
    light: 'Poppins-Light',
    semiBold: 'Poppins-SemiBold',
    bold: 'Poppins-Bold',
    extraBold: 'Poppins-ExtraBold',
    extraBoldItalic: 'Poppins-ExtraBoldItalic',
  },
  fontSegoe: {
    regular: 'Segoe',
    italic: 'Segoe-Italic',
    boldItalic: 'Segoe-BoldItalic',
    bold: 'Segoe-Bold',
  },
  fontSize: {
    small: moderateScale(13),
    semiMedium: moderateScale(15),
    medium: moderateScale(20),
    large: moderateScale(30),
    extra: moderateScale(35),
  },
};

/**
  * Configuration which responsible for responsive beside react-native-screen-matter modules
*/

let screenWidth = Dimensions.get('window').width;
let screenHeight = Dimensions.get('window').height;

const widthPercentageToDP = (widthPercent) => {
  const elemWidth =
    typeof widthPercent === 'number' ? widthPercent : parseFloat(widthPercent);

  // Use PixelRatio.roundToNearestPixel method in order to round the layout
  // size (dp) to the nearest one that correspons to an integer number of pixels.
  return PixelRatio.roundToNearestPixel((screenWidth * elemWidth) / 100);
};

const heightPercentageToDP = (heightPercent) => {
  const elemHeight =
    typeof heightPercent === 'number'
      ? heightPercent
      : parseFloat(heightPercent);

  // Use PixelRatio.roundToNearestPixel method in order to round the layout
  // size (dp) to the nearest one that correspons to an integer number of pixels.
  return PixelRatio.roundToNearestPixel((screenHeight * elemHeight) / 100);
};

const standardLength =
  screenWidth > screenHeight
    ? screenWidth
    : screenHeight > 750
    ? screenHeight - screenHeight * 0.15
    : screenHeight;
const offset =
  screenWidth > screenHeight
    ? 0
    : Platform.OS === 'ios'
    ? 78
    : StatusBar.currentHeight; // iPhone X style SafeAreaView size in portrait

const deviceHeight =
  Platform.OS === 'android' ? standardLength - offset : standardLength;

function RFPercentage(percent) {
  const heightPercent = (percent * deviceHeight) / 100;
  return Math.round(heightPercent);
}

export {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  RFPercentage as fp,
  AppStyle,
};
