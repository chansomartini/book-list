import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import {useSelector, useDispatch} from 'react-redux';
import {View} from 'react-native';
import {navigationRef} from '../../features/notification/RootNavigation';
import remoteConfig from '@react-native-firebase/remote-config';
import analytics from '@react-native-firebase/analytics';

// Pages
import {
	Login,
	Register,
	Dashboard,
	BookDetail,
	RemotePushController,
	Promo,
} from '../../features';

/**
 * Config for header on pages
 */

const configDashboard = {
	headerTitleStyle: {textAlign: 'center', marginLeft: 56},
	style: {shadowColor: 'transparent'},
	headerTintColor: '#222',
	headerRight: () => (
		<Icon name="filter-alt" size={30} color="#222" style={{marginRight: 10}} />
	),
	headerStyle: {
		backgroundColor: '#f2f2f2',
		elevation: 0,
	},
};

const configRegister = {
	title: false,
	style: {shadowColor: 'transparent'},
	headerTintColor: '#05ba74',
	headerStyle: {
		backgroundColor: '#f2f2f2',
		elevation: 0,
	},
};
const configBookDetail = {
	title: 'Book Detail',
	headerTitleStyle: {textAlign: 'center', marginRight: 56},
	style: {shadowColor: 'transparent'},
	headerStyle: {
		backgroundColor: '#f2f2f2',
		elevation: 0,
	},
};

// end of Config for header on pages

const Stack = createStackNavigator();
const Navigator = (props) => {
	const dispatch = useDispatch();
	const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
	const [title, setTitle] = useState('Venus Bookstore');

	/**
		* function for remote config firebase, when data update on Firebase. title on dashboard will changed
	*/

	useEffect(() => {
		async function fetchData() {
			await remoteConfig().setConfigSettings({
				minimumFetchIntervalMillis: 0,
			});
			await remoteConfig().fetch(60);
			remoteConfig()
				.fetchAndActivate()
				.then((data) => {
					const remoteTitle = remoteConfig()
						.getValue('title_bookstore')
						.asString();
					console.log(remoteTitle);
					setTitle(remoteTitle);
				})
				.catch((err) => {
					console.log(err);
					crashlytics().log('Update_remote_config')
				});
		}

		fetchData();
	}, []);

	// Config for deeplink

	const url = {
		prefixes: ['booklist://'], // Main url
		config: {
			screens: {
				promo: 'promo/:id', // Screen url
				bookDetail: 'bookDetail/:id', // Screen url
			},
		},
	};
	// end of config deeplink

	return (
		<NavigationContainer ref={navigationRef} linking={url}>
			<Stack.Navigator>
				{isLoggedIn ? (
					<>
						<Stack.Screen
							name="dashboard"
							component={Dashboard}
							options={{...configDashboard, title: title}}
						/>
						<Stack.Screen
							name="bookDetail"
							component={BookDetail}
							options={configBookDetail}
							path="bookDetail"
						/>
						<Stack.Screen
							name="promo"
							component={Promo}
							options={{headerShown: false}}
						/>
					</>
				) : (
					<>
						<Stack.Screen
							name="auth"
							component={Auth}
							options={{headerShown: false}}
						/>
					</>
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
};

export default Navigator;

const Auth = (props) => {
	return (
		<Stack.Navigator>
			<Stack.Screen
				name="login"
				component={Login}
				options={{headerShown: false}}
			/>
			<Stack.Screen
				name="register"
				component={Register}
				options={configRegister}
			/>
		</Stack.Navigator>
	);
};
