import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import {styles} from './style';

	/**
		* Component Loading indicator and reuseable for entire app
	*/


const Loading = () => {
	return (
		<View style={styles.loadingWrapper}>
			<ActivityIndicator size="large" color="#222" style={styles.loading} />
			<Text style={styles.textLoading}>Loading please wait ...</Text>
		</View>
	);
};

export default Loading;
