import {StyleSheet} from 'react-native';
import {AppStyle, wp, hp} from '../../styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
	loadingWrapper: {
		alignSelf: 'center',
		justifyContent: 'center',
		position: 'absolute',
		top: hp(30),
	},
	loading: {
		alignSelf: 'center',
		justifyContent: 'center',
	},
	textLoading: {
		marginVertical: hp(2),
		fontSize: fontSize.semiMedium,
		fontFamily: fontSegoe.bold,
	},
});
