import React, {useState} from 'react';
import {
	Text,
	View,
	FlatList,
	LayoutAnimation,
	Platform,
	UIManager,
} from 'react-native';
import SplitTitle from '../splitTitle';
import {styles} from './style';

/**
	* This component responsible as Carousel and can be use on entire app. Which simple config with passing parram from parent
	* exp. 
		<Carousel
			data={this.state.categories} // >> Which data you want to input
			renderItem={this.renderCategories} // >> Render function to styling the look of carousel
			keyExtractor={this.keyExtractorCategories} // >> Key for mapping
			title="Top Categories" // >> Main Tittle Carousel
			button="VIEW ALL" // >> Title for Upper Rigth button on Carousel beside Main Title
			showsHorizontalScrollIndicator={false}
			...flatlistConfig // >> pass the other flatlist property as usual 
		/>
*/

class Carousel extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const {
			title,
			button,
			onPressExpand,
			data,
			renderItem,
			keyExtractor,
		} = this.props;
		return (
			<View style={styles.wrapper}>
				<SplitTitle title={title} button={button} onPress={onPressExpand} />
				<FlatList
					data={data}
					horizontal={true}
					renderItem={renderItem}
					keyExtractor={keyExtractor}
					showsHorizontalScrollIndicator={false}
				/>
			</View>
		);
	}
}

export default Carousel;
