import {StyleSheet} from 'react-native';
import {hp} from '../../styles/styles';

export const styles = StyleSheet.create({
	wrapper : {
		marginVertical : hp(1)
	}
});
