import {StyleSheet} from 'react-native';
import {AppStyle, wp, hp, fp} from '../../styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
	title: {
		fontFamily: fontSegoe.regular,
		fontSize: fontSize.semiMedium,
	},
	btn: {
		fontFamily: fontSegoe.bold,
		fontSize: fontSize.small,
		color: color.blue,
	},
	wrapper : {
		flexDirection :'row',
		justifyContent : 'space-between',
		marginVertical : hp(1.5)
	}
});
