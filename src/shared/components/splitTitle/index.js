import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {styles} from './style';

/**
	* Component for Title + Button upper the Carousel
	* exp. 
		<SplitTitle
			title={this.state.focus ? 'All Books' : 'Top New Releases'} // >>  Main Title
			button={this.state.focus ? 'VIEW LESS' : 'VIEW ALL'} // >> Right button title
			onPress={() => this.animationHandler()} // >> Onpress Function when right button press
		/>
*/

const SplitTitle = ({title, button, onPress}) => {
	return (
		<View style={styles.wrapper}>
			<Text style={styles.title}>{title}</Text>
			<TouchableOpacity onPress={onPress}>
				<Text style={styles.btn}>{button}</Text>
			</TouchableOpacity>
		</View>
	);
};

export default SplitTitle;
