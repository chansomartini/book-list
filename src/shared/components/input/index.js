import React from 'react';
import {
	View,
	Text,
	TextInput,
	LayoutAnimation,
	Platform,
	TouchableOpacity,
	UIManager,
	StyleSheet,
} from 'react-native';
import {styles} from './style';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import {AppStyle, wp, hp} from '../../styles/styles';
const {color, fontSize} = AppStyle;

/**
	* This component responsible as textinput with simple animation effect and can be reuseable on entire app. Which simple config with passing parram from parent
	* exp. 
		<Input
			type="PASSWORD" // >> Label for input
			value={this.state.password} // >> Default Value for input 
			onChangeText={(text) => this.setState({password: text})} // >> When type data update to state
			secureTextEntry={true} // >> Invisible text when typing usually used on password or any credential
		/>
*/

class Input extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			focus: false,
		};
	}


	/**
		* Function for animation handler, when input pressed animation run as preset.
		* Animation can reconfig by simple setting the presets 'LayoutAnimation.Presets.spring'
	*/

	animationHandler = () => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
		this.setState({focus: !this.state.focus});
	};

	render() {

	/**
		* Conditional Platform for activate animation
	*/
		
		if (
			Platform.OS === 'android' &&
			UIManager.setLayoutAnimationEnabledExperimental
		) {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}

		return (
			<TouchableOpacity
				onPress={() => this.animationHandler()}
				style={
					this.state.focus ? styles.containerActive : styles.containerInactive
				}>
				{this.state.focus ? (
					<View>
						<Text style={styles.expandTitle}>{this.props.type}</Text>
						<View style={styles.flexComponent}>
							<Icon
								name={
									this.props.type == 'EMAIL'
										? 'email'
										: this.props.type == 'PASSWORD'
										? 'lock'
										: this.props.type == 'NAME'
										? 'account-circle'
										: this.props.type == 'PHONE'
										? 'phone'
										: 'lock'
								}
								size={fontSize.medium}
								color={color.primary}
								style={styles.icon}
							/>
							<TextInput
								placeholder={
									this.props.value != null
										? this.props.value
										: this.props.type == 'PASSWORD'
										? this.props.type
										: this.props.type
								}
								value={this.props.value}
								onChangeText={(text) => this.props.onChangeText(text)}
								onSubmitEditing={() => this.setState({focus: false})}
								blurOnSubmit={false}
								returnKeyType="next"
								placeholderTextColor={color.primary}
								style={styles.textInput}
								secureTextEntry={this.props.secureTextEntry}
								autoFocus={true}
							/>
						</View>
					</View>
				) : (
					<View style={styles.flexComponent}>
						<Icon
							name={
								this.props.type == 'EMAIL'
									? 'email'
									: this.props.type == 'PASSWORD'
									? 'lock'
									: this.props.type == 'NAME'
									? 'account-circle'
									: this.props.type == 'PHONE'
									? 'phone'
									: 'lock'
							}
							size={fontSize.medium}
							color={color.dark}
						/>
						<Text style={styles.title}>
							{this.props.secureTextEntry && this.props.value !== null
								? '*******'
								: !this.props.secureTextEntry && this.props.value !== null
								? this.props.value
								: this.props.type}
						</Text>
					</View>
				)}
			</TouchableOpacity>
		);
	}
}

export default Input;
