import {StyleSheet} from 'react-native';
import {AppStyle, hp} from '../../styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
	btnPrimary: {
		backgroundColor: color.primary,
		width: '100%',
		height: hp(7),
		borderRadius: 10,
		alignItems: 'center',
		justifyContent: 'center',
		marginVertical: hp(4),
	},
	btnCancel: {
		backgroundColor: color.red,
		width: '100%',
		height: hp(7),
		borderRadius: 10,
		alignItems: 'center',
		justifyContent: 'center',
		marginVertical: hp(-2),
	},
	txtPrimary: {
		letterSpacing: 2,
		fontSize: fontSize.semiMedium,
		color: color.light,
		fontFamily: fontSegoe.regular,
	},
});
