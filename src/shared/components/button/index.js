import React from 'react';
import {Text, View, TouchableOpacity, ActivityIndicator} from 'react-native';
import {styles} from './style';

/**
	* This component responsible as Main Button and can be use on entire app. Which simple config with passing parram from parent
	* exp. 
		<Button
		cancel // >> Cancel type to change button color (default is primary color)
		loading={this.props.isLoading} // >> If activity indicator and loading is needed when button pressed, pass loading state
		title="LOGIN" // >> Title for button
		onPress={this.handleLogin} // >> onPress function
		/>
*/

class Button extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<TouchableOpacity
				onPress={this.props.onPress}
				style={this.props.cancel ? styles.btnCancel : styles.btnPrimary}>
				{this.props.loading ? (
					<ActivityIndicator size="large" color="#fff" />
				) : (
					<Text style={styles.txtPrimary}>{this.props.title}</Text>
				)}
			</TouchableOpacity>
		);
	}
}

export default Button;
