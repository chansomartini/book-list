import Input from './input'
import Button from './button'
import SplitTitle from './splitTitle'
import Carousel from './carousel'
import Loading from './loading'


export {
	Input,
	Button,
	SplitTitle,
	Carousel,
	Loading
}