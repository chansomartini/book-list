import AsyncStorage from '@react-native-async-storage/async-storage';

export async function getHeaders() {

	/**
		* Function to load data you have been save before to local
		* AsyncStorage.getItem('KEY_NAME');
	*/

	const token = await AsyncStorage.getItem('APP_AUTH_TOKEN');

	return {
		'Content-Type': 'application/json',
		Authorization: 'Bearer ' + token,
	};
}


/**
	* Function to Save data locally
	* AsyncStorage.setItem('KEY_NAME', data_to_save); 
*/

export async function saveToken(token) {
	AsyncStorage.setItem('APP_AUTH_TOKEN', token);
}
