import * as React from 'react'

export const navigationRef = React.createRef();

/**
	* function to navigate notification to deeplink without facing error on navigation
*/

export function navigate(name, params) {
  navigationRef.current?.navigate(name, params);
}