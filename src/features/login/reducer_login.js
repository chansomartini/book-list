import {
	LOGIN,
	LOGIN_SUCCESS,
	LOGIN_FAILED,
	REGISTER,
	REGISTER_SUCCESS,
	REGISTER_FAILED,
} from './action_login';

const initialState = {
	isLoading: false,
	isLoggedIn: false,
	registerSuccess : false
};

const auth = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN: {
			return {
				...state,
				isLoading: true,
			};
		}
		case LOGIN_SUCCESS: {
			return {
				...state,
				isLoading: false,
				isLoggedIn: true,
			};
		}
		case LOGIN_FAILED: {
			return {
				...state,
				isLoading: false,
				isLoggedIn: false,
			};
		}
		case REGISTER: {
			return {
				...state,
				isLoading: true,
			};
		}
		case REGISTER_SUCCESS: {
			return {
				...state,
				isLoading: false,
				registerSuccess: true
			};
		}
		case REGISTER_FAILED: {
			return {
				...state,
				isLoading: false,
				registerSuccess: false
			};
		}
		default:
			return state;
	}
};

export default auth;
