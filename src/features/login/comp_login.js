import React from 'react';
import {
	Text,
	View,
	StyleSheet,
	TextInput,
	KeyboardAvoidingView,
	SafeAreaView,
} from 'react-native';
import {styles} from './style_login';
import {Input, Button} from '../../shared/components';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

export class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: 'admin@admin.com',
			password: 'admin2020',
		};
	}

	handleCreateAccount = () => {
		this.props.navigation.navigate('register');
	};

	handleLogin = () => {
		const data = {
			email: this.state.email,
			password: this.state.password,
		};
		this.props.loginAction(data);
	};

	render() {
		return (
			<KeyboardAvoidingView
				behavior={Platform.OS === 'ios' ? 'padding' : null}
				style={{flex: 1}}>
				<SafeAreaView style={styles.container}>
					<Icon name="account-circle" size={100} color="rgba(34,34,34,0.4)" />
					<Text style={styles.title}>Welcome Back</Text>
					<Text style={styles.subTitle}>Sign in to continue</Text>
					<Input
						type="EMAIL"
						value={this.state.email}
						onChangeText={(text) => this.setState({email: text})}
					/>
					<Input
						type="PASSWORD"
						value={this.state.password}
						onChangeText={(text) => this.setState({password: text})}
						secureTextEntry={true}
					/>
					<View style={styles.wrapperForgot}>
						<Text style={styles.forgotPassword}>Forgot Password?</Text>
					</View>
					<Button
						loading={this.props.isLoading}
						title="LOGIN"
						onPress={this.handleLogin}
					/>
					<Text style={styles.ask}>
						Don't have an account ?{' '}
						<Text onPress={this.handleCreateAccount} style={styles.action}>
							create a new account
						</Text>
					</Text>
				</SafeAreaView>
			</KeyboardAvoidingView>
		);
	}
}
