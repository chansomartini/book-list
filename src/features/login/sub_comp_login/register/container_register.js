import {connect} from 'react-redux';
import {Register} from './comp_register';
import {registerAction} from '../../action_login';

const mapStateToProps = (state) => {
	return {
		isLoading: state.auth.isLoading,
		registerSuccess: state.auth.registerSuccess,
	};
};

const mapDispatchToProps = {
	registerAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);