import React from 'react';
import {
	Text,
	View,
	StyleSheet,
	TextInput,
	KeyboardAvoidingView,
	SafeAreaView,
} from 'react-native';
import {styles} from './style_register';
import {Input, Button} from '../../../../shared/components';

export class Register extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			email: null,
			name: null,
			phone: null,
			password: null,
			confirmPassword: null,
		};
	}

	handleRegister = () => {
		const data = {
			name: this.state.name,
			email: this.state.email,
			nip: this.state.phone,
			confirmPassword: this.state.confirmPassword,
			password: this.state.password,
		};
		this.props.registerAction(data);
	};

	componentDidUpdate(prevProps) {
		if (this.props.registerSuccess !== prevProps.registerSuccess) {
			this.props.navigation.goBack();
		}
	}

	render() {
		return (
			<KeyboardAvoidingView
				behavior={Platform.OS === 'ios' ? 'padding' : null}
				style={{flex: 1}}>
				<SafeAreaView style={styles.container}>
					<Text style={styles.title}>Create Account</Text>
					<Text style={styles.subTitle}>Create a new account</Text>
					<Input
						type="NAME"
						value={this.state.name}
						onChangeText={(text) => this.setState({name: text})}
					/>
					<Input
						type="EMAIL"
						value={this.state.email}
						onChangeText={(text) => this.setState({email: text})}
					/>
					<Input
						type="PHONE"
						value={this.state.phone}
						onChangeText={(text) => this.setState({phone: text})}
					/>
					<Input
						type="PASSWORD"
						value={this.state.password}
						onChangeText={(text) => this.setState({password: text})}
						secureTextEntry={true}
					/>
					<Input
						type="CONFIRM PASSWORD"
						value={this.state.confirmPassword}
						onChangeText={(text) => this.setState({confirmPassword: text})}
						secureTextEntry={true}
					/>
					<Button
						loading={this.props.isLoading}
						title="CREATE ACCOUNT"
						onPress={this.handleRegister}
					/>
					<Text
						onPress={() => this.props.navigation.goBack()}
						style={styles.ask}>
						Already have an account ? <Text style={styles.action}>Login</Text>
					</Text>
				</SafeAreaView>
			</KeyboardAvoidingView>
		);
	}
}
