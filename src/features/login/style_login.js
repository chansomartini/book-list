import {StyleSheet} from 'react-native';
import {AppStyle, wp, hp, fp} from '../../shared/styles/styles';

const {fontSize,fontSegoe, fontPoppins,color} = AppStyle;

export const styles = StyleSheet.create({
	container: {
		padding: wp(10),
		alignItems: 'center',
		justifyContent:'center',
		height:fp(100),
		flex:1
	},
	title: {
		fontSize: fontSize.large,
		fontFamily: fontPoppins.bold,
	},
	subTitle: {
		fontSize: fontSize.semiMedium,
		fontFamily : fontSegoe.regular
	},
	forgotPassword : {
		fontSize: fontSize.small,
		color: color.primary,
		fontFamily : fontSegoe.bold,
		textAlign:'right'
	},
	wrapperForgot : {
		width:'100%'
	},
	ask : {
		fontSize: fontSize.small,
		fontFamily : fontSegoe.regular,
	},
	action : {
		fontFamily : fontSegoe.bold,
		color: color.primary
	}
});
