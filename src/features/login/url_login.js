import axios from 'axios';

const baseUrl = 'https://us-central1-principal-my.cloudfunctions.net/books/';

export const loginApi = (payload) => {
	return axios({
		method: 'POST',
		url: baseUrl + 'login',
		data: payload,
	});
};

export const registerApi = (payload) => {
	return axios({
		method: 'POST',
		url: baseUrl + 'register',
		data: payload,
	});
};
