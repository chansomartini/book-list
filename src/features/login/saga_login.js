import {
	LOGIN,
	LOGIN_SUCCESS,
	LOGIN_FAILED,
	REGISTER,
	REGISTER_SUCCESS,
	REGISTER_FAILED,
} from './action_login';
import {BOOKS} from '../dashboard/action_dashboard'
import {ToastAndroid} from 'react-native';
import {loginApi, registerApi} from './url_login';
import {takeLatest, put} from 'redux-saga/effects';
import {saveToken} from '../../shared/functions/asyncstorage';

function* login(action) {
	try {
		const resLogin = yield loginApi(action.payload);
		if (resLogin && resLogin.data) {
			yield saveToken(resLogin.data.data.token);
			yield put({type: LOGIN_SUCCESS});
			yield put({type: BOOKS});
		} else {
			yield put({type: LOGIN_FAILED});
		}
	} catch (err) {
		console.log(err);
		yield put({type: LOGIN_FAILED});
	}
}

function* register(action) {
	try {
		const resRegister = yield registerApi(action.payload);
		if (resRegister && resRegister.data) {
			ToastAndroid.showWithGravity(
				'Register Success !!',
				ToastAndroid.SHORT,
				ToastAndroid.CENTER,
			);
			yield put({type: REGISTER_SUCCESS});
		} else {
			ToastAndroid.showWithGravity(
				'Register Failed !!',
				ToastAndroid.SHORT,
				ToastAndroid.CENTER,
			);
			yield put({type: REGISTER_FAILED});
		}
	} catch (err) {
		ToastAndroid.showWithGravity(
			'Register Failed !!',
			ToastAndroid.SHORT,
			ToastAndroid.CENTER,
		);
		yield put({type: REGISTER_FAILED});
	}
}

function* saga_login() {
	yield takeLatest(LOGIN, login);
	yield takeLatest(REGISTER, register);
}

export default saga_login;
