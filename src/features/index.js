import Login from './login/container_login';
import Register from './login/sub_comp_login/register/container_register';
import Dashboard from './dashboard/container_dashboard'
import BookDetail from './dashboard/sub_comp_dashboard/book_detail/container_book_detail';
import RemotePushController from './notification/services_notification'
import Promo from './promo/comp_promo'

export {Login, Register, Dashboard, BookDetail, RemotePushController, Promo};
