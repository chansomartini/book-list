import {StyleSheet} from 'react-native'
import {AppStyle, wp, hp, fp} from '../../shared/styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
	container: {
		padding: wp(10),
		alignItems: 'center',
		justifyContent:'center',
		height:fp(100),
		flex:1
	},
	title: {
		fontFamily:fontSegoe.bold,
		fontSize: fontSize.medium,
		textAlign:'center'
	},
	subTitle: {
		fontFamily:fontSegoe.regular,
		fontSize: fontSize.medium,
		textAlign:'center'
	}
})