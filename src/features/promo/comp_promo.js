import React from 'react';
import {Text, View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {styles} from './style_promo';
import {Button} from '../../shared/components';

const Promo = ({navigation, route}) => {
	const dispatch = useDispatch()
	const isLoading = useSelector((state) => state.getBooks.isLoading);
	const handleGetBooks = (data) => {
		dispatch({type:'BOOK_DETAIL', payload: data})
		navigation.navigate('bookDetail')
	}
	return (
		<View style={styles.container}>
			<Text style={styles.title}>Dont Miss it !!</Text>
			<Text style={styles.subTitle}>Get our new collection </Text>
			<Button
				loading={isLoading}
				title="GET NEW BOOKS"
				onPress={() => handleGetBooks(route.params)}
			/>
			<Button
				cancel
				loading={isLoading}
				title="NOT INTEREST"
				onPress={() => navigation.navigate('dashboard')}
			/>
		</View>
	);
};

export default Promo;
