import {connect} from 'react-redux';
import {BookDetail} from './comp_book_detail';
import {bookDetailAction} from '../../action_dashboard'

const mapStateToProps = (state) => {
	return {
		data : state.getBooks.booksDetail,
		loading : state.getBooks.isLoading
	};
};

const mapDispatchToProps = {
	bookDetailAction
};

export default connect(mapStateToProps, mapDispatchToProps)(BookDetail);
