import React from 'react';
import {Text, View, Image, ActivityIndicator, ScrollView} from 'react-native';
import {Loading} from '../../../../shared/components';
import {Rating} from 'react-native-ratings';
import {styles} from './style_book_detail';
import {AppStyle} from '../../../../shared/styles/styles';
const {color} = AppStyle;
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

export class BookDetail extends React.Component {
	constructor(props) {
		super(props);
	}

	// Choose random number for review which not provide from API :)

	randomNumber = (min, max) => {
		const random = Math.random() * (max - min) + min;
		return Math.floor(random);
	};

	render() {
		const {coverImageUrl, title, author, synopsis, rating} = this.props.data;
		return (
			<View style={styles.container}>
				{this.props.data == null ? null : this.props.loading ? (
					<Loading />
				) : (
					<ScrollView showsVerticalScrollIndicator={false}>
						<Image source={{uri: coverImageUrl}} style={styles.bookCover} />
						<Text style={styles.title}>{title}</Text>
						<Text style={styles.author}>{author}</Text>
						<View style={styles.ratingWrapper}>
							<Rating
								type="custom"
								imageSize={20}
								readonly
								startingValue={rating / 2}
								ratingColor={color.red}
								ratingBackgroundColor="#999"
								tintColor={'#f2f2f2'}
							/>
							<Text style={styles.review}>
								{this.randomNumber(1, 100)} Reviews
							</Text>
							
						</View>
						<Text style={styles.synopsis}>{synopsis}</Text>
					</ScrollView>
				)}
			</View>
		);
	}
}
