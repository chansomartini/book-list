import {StyleSheet} from 'react-native';
import {AppStyle, wp, hp, fp} from '../../../../shared/styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
	container: {
		padding: wp(4),
		alignItems: 'center',
	},
	bookCover: {
		width: wp(50),
		height: hp(40),
		borderRadius: 5,
		resizeMode:'contain',
		alignSelf:'center'
	},
	title: {
		fontSize: fontSize.medium,
		fontFamily: fontSegoe.regular,
		marginVertical: hp(1),
		textAlign: 'center',
	},
	author: {
		fontSize: fontSize.small,
		fontFamily: fontSegoe.regular,
		color: '#555',
		textAlign:'center'
	},
	synopsis: {
		fontSize: fontSize.small,
		fontFamily: fontSegoe.regular,
		textAlign: 'justify',
		marginVertical: hp(5),
	},
	ratingWrapper: {
		flexDirection: 'row',
		marginVertical: hp(1),
		alignSelf:'center'
	},
	review: {
		fontSize: fontSize.small,
		fontFamily: fontSegoe.bold,
		marginHorizontal: wp(3),
		alignSelf: 'center',
		color: color.blue,
	},
});
