import {StyleSheet} from 'react-native';
import {AppStyle, wp, hp, fp} from '../../shared/styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
	container: {
		padding: wp(3),
	},
	containerBookListInFocus: {
		marginRight: wp(5),
		width: wp(30),
	},
	containerBookList: {
		marginHorizontal: wp(1),
		marginBottom:hp(2),
		width: wp(29),
	},
	bookCover: {
		width: wp(29),
		height: hp(25),
		borderRadius: 10,
		marginVertical: hp(1),
	},
	bookCoverInFocus: {
		width: wp(32),
		height: hp(25),
		borderRadius: 10,
		marginVertical: hp(1),
	},
	title: {
		fontFamily: fontSegoe.bold,
		fontSize: fontSize.small,
	},
	author: {
		fontFamily: fontSegoe.regular,
		fontSize: fontSize.small,
		marginVertical: hp(1)
	},
	pageCount: {
		fontFamily: fontSegoe.bold,
		fontSize: fontSize.small,
		color: color.primary,
	},
	containerCategories: {
		width: wp(40),
		height: hp(15),
		borderRadius: 10,
		marginHorizontal: wp(1),
		justifyContent: 'center',
		alignItems: 'center',
	},
	categories: {
		color: color.light,
		fontSize: fontSize.semiMedium,
		fontFamily: fontSegoe.bold,
	},
});
