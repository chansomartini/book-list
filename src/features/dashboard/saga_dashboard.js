import {
	BOOKS,
	BOOKS_SUCCESS,
	BOOKS_FAILED,
	BOOK_DETAIL,
	BOOK_DETAIL_SUCCESS,
	BOOK_DETAIL_FAILED,
} from './action_dashboard';
import {getBooksApi, getBookDetailsApi} from './url_dashboard';
import {takeLatest, put} from 'redux-saga/effects';
import {getHeaders} from '../../shared/functions/asyncstorage';

function* getBooks(action) {
	try {
		const headers = yield getHeaders();

		const resBooks = yield getBooksApi(headers);
		if (resBooks && resBooks.data) {
			yield put({type: BOOKS_SUCCESS, payload: resBooks.data.data});
		} else {
			yield put({type: BOOKS_FAILED});
		}
	} catch (err) {
		console.log(err);
		yield put({type: BOOKS_FAILED});
	}
}

function* bookDetails(action) {
	try {
		const headers = yield getHeaders();
		const resBookDetail = yield getBookDetailsApi(action.payload, headers);
		if (resBookDetail && resBookDetail.data.data) {
			yield put({type: BOOK_DETAIL_SUCCESS, payload: resBookDetail.data.data});
		} else {
			yield put({type: BOOK_DETAIL_FAILED});
		}
	} catch (err) {
		yield put({type: BOOK_DETAIL_FAILED});
	}
}

function* saga_dashboard() {
	yield takeLatest(BOOKS, getBooks);
	yield takeLatest(BOOK_DETAIL, bookDetails);
}

export default saga_dashboard;
