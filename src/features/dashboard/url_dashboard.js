import axios from 'axios';

const baseUrl = 'https://us-central1-principal-my.cloudfunctions.net/books/';

export const getBooksApi = (headers) => {
	return axios({
		method: 'GET',
		url: baseUrl + 'books',
		headers
	});
};

export const getBookDetailsApi = (id, headers) => {
	return axios({
		method: 'GET',
		url: baseUrl + 'books/' + id,
		headers
	});
};
