import React from 'react';
import {
	Text,
	View,
	FlatList,
	Image,
	ActivityIndicator,
	LayoutAnimation,
	Platform,
	UIManager,
	ScrollView,
	TouchableOpacity,
} from 'react-native';
import {styles} from './style_dashboard';
import {Carousel, SplitTitle, Loading} from '../../shared/components';
import {LogBox} from 'react-native';
import {AppStyle} from '../../shared/styles/styles';
import analytics from '@react-native-firebase/analytics';

const {color} = AppStyle;

export class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			categories: [
				'Drama',
				'Photography',
				'History',
				'Fantasy',
				'Science Fiction',
				'Biography',
			],
			colors: [
				color.purple,
				color.blue,
				color.red,
				color.purple,
				color.blue,
				color.red,
			],
			focus: false,
		};
	}

	// Remove Warning Cause ScrollView nested flatlist (find solution ASAP)
	componentDidMount() {
		LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
	}

	// analytics Button
	analyticsBooksLog = (id, title, author) => {
		analytics().logEvent('books', {
			id: id,
			title: title,
			author: author,
		})
		.then(() => {
			console.log('log saved')
		})
		.catch((err) => {
			console.log(err)
		})
	};

	// button book detail
	bookDetailHandler = (id, title, author) => {
		this.analyticsBooksLog(id, title, author);
		this.props.bookDetailAction(id);
		this.props.navigation.navigate('bookDetail');
	};

	// Books Flatlist Render
	keyExtractorBooks = (item) => item.id;
	renderBooks = ({item}) => (
		<TouchableOpacity
			onPress={() => this.bookDetailHandler(item.id, item.title, item.author)}
			style={
				this.state.focus
					? styles.containerBookList
					: styles.containerBookListInFocus
			}>
			<Image
				source={{uri: item.coverImageUrl}}
				style={this.state.focus ? styles.bookCover : styles.bookCoverInFocus}
			/>
			<Text numberOfLines={1} style={styles.title}>
				{item.title}
			</Text>
			<Text numberOfLines={1} style={styles.author}>
				{item.author}
			</Text>
			<Text style={styles.pageCount}>$ {item.price}</Text>
		</TouchableOpacity>
	);

	// Categories Flatlist Render
	keyExtractorCategories = (item) => item;
	renderCategories = ({item, index}) => (
		<View
			style={{
				...styles.containerCategories,
				backgroundColor: this.state.colors[index],
			}}>
			<Text style={styles.categories}>{item}</Text>
		</View>
	);

	// Animation Toggler
	animationHandler = () => {
		LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
		this.setState({focus: !this.state.focus});
	};

	render() {
		if (
			Platform.OS === 'android' &&
			UIManager.setLayoutAnimationEnabledExperimental
		) {
			UIManager.setLayoutAnimationEnabledExperimental(true);
		}
		return (
			<View style={styles.container}>
				{this.props.isLoading ? (
					<Loading />
				) : (
					<ScrollView showsVerticalScrollIndicator={false}>
						<SplitTitle
							title={this.state.focus ? 'All Books' : 'Top New Releases'}
							button={this.state.focus ? 'VIEW LESS' : 'VIEW ALL'}
							onPress={() => this.animationHandler()}
						/>
						<FlatList
							key={this.state.focus ? 1 : 0}
							data={
								this.state.focus ? this.props.data : this.props.topNewRelease
							}
							numColumns={this.state.focus ? 3 : false}
							horizontal={this.state.focus ? false : true}
							renderItem={this.renderBooks}
							keyExtractor={this.keyExtractorBooks}
							showsHorizontalScrollIndicator={false}
							showsVerticalScrollIndicator={false}
						/>
						<Carousel
							data={this.state.categories}
							renderItem={this.renderCategories}
							keyExtractor={this.keyExtractorCategories}
							title="Top Categories"
							button="VIEW ALL"
						/>
						<Carousel
							data={this.props.topNewRelease}
							renderItem={this.renderBooks}
							keyExtractor={this.keyExtractorBooks}
							title="Top New Releases"
							button="VIEW ALL"
						/>
					</ScrollView>
				)}
			</View>
		);
	}
}
