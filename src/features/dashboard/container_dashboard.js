import {connect} from 'react-redux';
import {Dashboard} from './comp_dashboard';
import {bookDetailAction} from './action_dashboard';

const mapStateToProps = (state) => {
	return {
		isLoading: state.getBooks.isLoading,
		data: state.getBooks.books,
		topNewRelease: state.getBooks.topNewRelease,
	};
};

const mapDispatchToProps = {
	bookDetailAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
