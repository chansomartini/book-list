/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

const NotificationHandler = async (message) => {
  console.log('RNFirebaseBackgroundMessage: ', message);
  return Promise.resolve();
};

AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerHeadlessTask(
  'ReactNativeFirebaseMessagingHeadlessTask',
  () => NotificationHandler,
);
