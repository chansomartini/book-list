/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {StatusBar} from 'react-native';

import {Provider} from 'react-redux';
import store from './src/shared/store/store';
import Navigator from './src/shared/navigation';
import {RemotePushController} from './src/features';
import remoteConfig from '@react-native-firebase/remote-config';

const App: () => React$Node = () => {

	return (
		<Provider store={store}>
			<StatusBar barStyle="light-content" />
			<Navigator  />
			<RemotePushController />
		</Provider>
	);
};

export default App;
